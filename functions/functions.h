//
// Created by ch on 16/02/2021.
//

//
// Macros
//
#define clear() printf("%c[2K", 27)
#define gotoxy(x,y) printf("\033[%d;%dH", (y), (x))

#ifndef MASTERMIND_FUNCTIONS_H
#define MASTERMIND_FUNCTIONS_H

void PrintGame(int tries, char* indicator, int guess[5]);
int FindElement(int element, const int *arr, int size);

#endif //MASTERMIND_FUNCTIONS_H
