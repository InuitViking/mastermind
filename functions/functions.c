#include <stdio.h>

//
// Created by ch on 16/02/2021.
//

//
// Functions
//
void PrintGame(int tries, char* indicator, int guess[5]){
	printf("Tries: %d\n", tries);
	printf("Indicator: %s\n",indicator);
	printf("Previous:  %d%d%d%d%d\n", guess[0],guess[1],guess[2],guess[3],guess[4]);	// I got lazy, okay?!
}

int FindElement(int element, const int *arr, int size) {
	for(int i=0; i<size;i++) {
		if (element == arr[i]) {
			return 1;
		}
	}
	return 0;
}