#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <memory.h>
#include <unistd.h>
#include "functions/functions.h"

/*
 * MasterMind
 * Make a MasterMind game with the following rules:
 * 1. The goal is to guess the correct code, which either an opponent or the program has generated
 * 2. The code contains five different numbers, from one to six
 * 3. You have eight tried
 * 4. When you've made a guess, then the game will maybe show a
 * - * - to mark that both the number and placement is correct
 * - o - to mark that the number is correct but the placement is wrong
 */

//
// Code
//
int main() {

	// Variables
	int code[5];									// Array to hold the code
	int guess[5] = {0,0,0,0,0};						// Array to hold the guess
	char indicator[5] = {'-', '-', '-', '-', '-'};	// Array to hold indicators
	int tries = 0;									// Store the amount of tries the user has had
	int answer;
	time_t t;										// Used for the random number seeder
	srand((unsigned) time(&t));						// Start the random number generator

	// Introduce the player to the game
	printf("### MASTERMIND ###\n"
		"Guess the five digit code with numbers between 1 and 6.\n"
		"You've got 8 tries.\n"
		"If you get a number and its location right, it will write *,\n"
		"If you get a number right but its location wrong, it will write o.\n"
		"Do you want the computer to make a random code or write in your own code?\n"
		"Random: 1 key\n"
		"Self: 2 key\n");

	// WHAT THE FUCK A GOTO!?!?!1?!?!1?+1?!
	// Yes, it's cleaner than looping and breaking (breaks and continues are gotos anyway ;))
	answer:
	// Read answer
	scanf("%d", &answer);

	// If answer is greater than 2 or lower than 1, say it's wrong and go back.
	if (answer > 2 || answer < 1){
		printf("Wrong! Try again!");
		goto answer;
	}

	// If answer is 1, generate a code for the user
	if(answer == 1){

		// Generate code - This generates 1 to 5, including 1 and 5
		for(int i=0;i<5;i++){
			code[i] = rand() % (6-1)+1;
		}

		// Tell the user the code has been generated and print it
		printf("The code has been generated.\n");
	}else{
		printf("This feature has not yet been implemented.");
		exit(0);
	}

	system("clear");

	while(memcmp(guess,code, 3 * sizeof(int)) != 0 && tries < 8){

		PrintGame(tries, indicator, guess);

		for(int i=0;i<5;i++){
			int guessInt;
			guessAgain:

			scanf("%d", &guessInt);

			if(guessInt > 5 || guessInt < 1){
				printf("It has be between 1 and 5.\n");
				goto guessAgain;
			}

			guess[i] = guessInt;
		}

		for(int i=0;i<5;i++){
			if(code[i] == guess[i]){
				indicator[i] = '*';
			}else if(FindElement(guess[i],code,5) != 0){
				indicator[i] = 'o';
			}else{
				indicator[i] = '-';
			}
		}

		if(memcmp(guess,code, 3 * sizeof(int)) == 0){
			printf("You guessed correctly!\n");
		}else{
			printf("Wrong!\n");
			sleep(1);
			system("clear");
		}

		tries++;
	}

	if(tries == 8){
		printf("You were wrong! The correct answer was:\n");
		for(int i=0;i<5;i++){
			printf("%d",code[i]);
		}
		printf("\n");
	}

	return 0;
}